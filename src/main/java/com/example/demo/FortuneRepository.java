package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by azwickey on 1/12/18.
 */
public interface FortuneRepository extends CrudRepository<Fortune,Long> {

    @Query("SELECT f FROM Fortune f WHERE f.text LIKE %:searchByFortuneText%")
    public List<Fortune> searchByFortuneText(String searchByFortuneText);
}