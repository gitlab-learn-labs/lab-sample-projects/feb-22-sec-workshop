package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
@RestController
public class DemoApplication implements CommandLineRunner {

	private static Logger LOG = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	private BackendController controller;

	@Override
	public void run(String... args) {
		try {
			LOG.info("Loading fortunes...");
			if(!controller.all().isEmpty()) return;

			controller.save(new Fortune("People are naturally attracted to you."));
			controller.save(new Fortune("You learn from your mistakes... You will learn a lot today."));
			controller.save(new Fortune("What ever your goal is in life, embrace it visualize it, and for it will be yours."));
			controller.save(new Fortune("Your shoes will make you happy today."));
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}