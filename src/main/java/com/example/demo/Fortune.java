package com.example.demo;

import jakarta.persistence.*;

@Entity
@Table(name = "fortune")
public class Fortune {

    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String text;

    public Fortune() {  }

    public Fortune(String text) {
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text.toUpperCase();
    }

    public String toString() {
        return getId() + ", " + getText();
    }
}