### Create a merge request to add security scans to our pipeline

### Theme

This section focuses on shifting left as a security practice and how your code changes will display security results after a commit rather than months down the line

# Step 1: Adding Security Scans

1. First make sure that you are on the main page of the project you just forked in. It is best if you have these instructions open in a separate tab/screen while completing the tasks.
  
2. Once ready use the left hand navigation menu to click through **Build \> Pipeline editor**. Here you will see the current set up of our main branch pipeline. Notice that there are two stages, build and test
  
3. This pipeline does nothing in terms of security scanning and only has a simple build job defined that packages our application and container. Lets go ahead and create a new branch to add out changes. Use the left hand navigation menu to click through **Code \> Branches** then click **New branch**. Name the branch **_secure-pipeline_** and make sure it is based off of **_main_**, then click **Create Branch**.
  
4. Once again use the left hand navigation menu to click through **Build \> Pipeline editor** to get back to the editor page. Make sure that the branch dropdown in the top left of the editor view shows **_secure-pipeline_**, or select the branch if it doesn't. We then want to add the additional pipeline yaml to the existing pipeline at the end of the file:

```plaintext
include:
  - template: Code-Quality.gitlab-ci.yml

ext-security-dependency:
  stage: test
  image: python:alpine3.18
  variables:
    SNYK_RESULT_JSON: data/snyk-dependency-1.json
    OUTPUT_JSON: external-security-results.json
  script:
    - python3 converters/snyk/snyk-dependency.py
  artifacts:
    reports:
      dependency_scanning: external-security-results.json
      cyclonedx: data/gl-sbom.cdx.json
    paths:
      - data/gl-sbom.cdx.json

ext-security-container:
  stage: test
  image: python:alpine3.18
  variables:
    SNYK_RESULT_JSON: data/snyk-container-1.json
    OUTPUT_JSON: external-security-results.json
  script:
    - python3 converters/snyk/snyk-container.py
  artifacts:
    reports:
      container_scanning: external-security-results.json

ext-security-sast:
  stage: test
  variables:
    SNYK_RESULT_JSON: data/snyk-sast-1.json
    OUTPUT_JSON: external-security-results.json
  before_script:
    - wget -O sarif-converter https://gitlab.com/ignis-build/sarif-converter/-/releases/v0.7.1/downloads/bin/sarif-converter-linux
    - chmod +x sarif-converter
  script:
    - ./sarif-converter --type sast $SNYK_RESULT_JSON $OUTPUT_JSON
  artifacts:
    reports:
      sast: external-security-results.json
```

5. First looking at the **_ext-security-..._** sections you can see that a number of security jobs have now been included. These define jobs that will utilize results from an external security scanner, Snyk, and integrate them into GitLab.  Because we are not using live Snyk scans, you can view the scanner outputs within the **_/data/_** directory.  The specific scanner result each job utilizes is defined by the `SNYK_RESULT_JSON` variable.

6. Looking at the include section, you can see that for illustrative purposes we have included a code quality template.  Common jobs, such as security scanning, can be templatized and automatically brough into our project.  These define different scans and jobs that will now be ran based off of our stages. To get a better look into the templates you can click Full configuration which will show the true pipeline yaml with all of the templates brought in. You can also click the branch icon in the top left to then click into a specific template to get its definition.

7. Click on the **Edit** tab again to be brought back to our normal editor.  For this workshop, in order to speed up our pipelines, we will remove the code quality template.  **REMOVE** these 2 lines from your pipeline:

```plaintext
include:
  - template: Code-Quality.gitlab-ci.yml
```

8. In case you had trouble following the above steps, your full **.gitlab-ci.yml** file should look like this:
```plaintext
image: docker:latest

stages:
  - build
  - test

variables:
  CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"

cache:
  key: MAVEN_CACHE
  paths:
    - .m2/repository

build:
  stage: build
  services:
  - docker:dind
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  before_script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $IMAGE .
  after_script:
    - docker push $IMAGE
  allow_failure: true

ext-security-dependency:
  stage: test
  image: python:alpine3.18
  variables:
    SNYK_RESULT_JSON: data/snyk-dependency-1.json
    OUTPUT_JSON: external-security-results.json
  script:
    - python3 converters/snyk/snyk-dependency.py
  artifacts:
    reports:
      dependency_scanning: external-security-results.json
      cyclonedx: data/gl-sbom.cdx.json
    paths:
      - data/gl-sbom.cdx.json

ext-security-container:
  stage: test
  image: python:alpine3.18
  variables:
    SNYK_RESULT_JSON: data/snyk-container-1.json
    OUTPUT_JSON: external-security-results.json
  script:
    - python3 converters/snyk/snyk-container.py
  artifacts:
    reports:
      container_scanning: external-security-results.json
      
ext-security-sast:
  stage: test
  variables:
    SNYK_RESULT_JSON: data/snyk-sast-1.json
    OUTPUT_JSON: external-security-results.json
  before_script:
    - wget -O sarif-converter https://gitlab.com/ignis-build/sarif-converter/-/releases/v0.7.1/downloads/bin/sarif-converter-linux
    - chmod +x sarif-converter
  script:
    - ./sarif-converter --type sast $SNYK_RESULT_JSON $OUTPUT_JSON
  artifacts:
    reports:
      sast: external-security-results.json
```

9. Now that our changes are in lets click **Commit changes** at the bottom of the page.

> If you run into any issues you can use the left hand navigation menu to click through **CI/CD -\> Pipelines**, click **Run pipeline**, select **_security-workshop-pipeline_** and click **Run pipeline** once again.
  
#  Step 3: Creating the Merge Request

1. Now to actually merge in the new pipeline we want to use the left hand navigation menu to click through **Code \> Branches** & then click **Merge request** in the secure-pipeline section. **ENSURE YOU HAVE REMOVED THE FORK RELATIONSHIP BEFORE DOING THIS**
  
2. On the resulting page scroll down to the **_Merge options_** and uncheck **Delete source branch when merge request is accepted**. You can leave the rest of the settings as is then click **Create merge request**.
  
3. First resolve any merge conflicts that may exist, but you should see a pipeline kick off. If you click the hyperlink it will bring you to the pipeline view where you can see all of the various jobs that we added to our yaml file. While this runs we are going to move forward to set up our compliance framework pipeline but we will check back in a bit to see the results of our scanners.
