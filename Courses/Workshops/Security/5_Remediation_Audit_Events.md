### We will wrap up the session remdiating a large number of the vulnerabilities that are detected in our container scanning.

### Theme

In case of security risks events, these tools are extremely handy at building a seamless yet auditable workflow for remediation.

# Step 1: Vulnerability Remediation Workflow

1. We have already seen that our project contains hundred of vulnerabilities.  Use the left hand navigation menu and click through **Secure -\> Vulnerability Report** to view the full report.

2. First change the _All tools_ section under **Tools** to just filter on Container Scanning. We can see the vast majority if vulnerabilities come from our container image.

3. Use the **Group By** toggle and group the findings by **Severity**.  Expand the `Critical` section and check the selection box for the 3 critical vulnerabilities.  Change the **_Status_** dropdown to `Confirm`, add a comment: `Investigate a more secure base image`, and click **_Change status_**. 

4. Click on any of the critical vulnerabilities.  At the bottom of the details page click **Create Issue**.  On the resulting Issue creation page, at the bottom of the **Issue Decription** remove `/confidential` from the description and also uncheck `This issue is confidential and should only be visible to team members with at least Reporter access.`, and click **Create Issue**.

5. Once the issue is created you will be taken to the Issue Details page. Click the **_Create Merge Request_** button.

6. On the Merge Request creation page, uncheck `Mark as Draft`, uncheck **_Delete source branch when merge request is accepted_**, and click **Create merge request**


# Step 2: Fix Vulnerable Container Image

1. Now we want to update our application to use a more secure container image.  From the Merge Reqest Details page lets go ahead and click **Open in Web IDE** in the **Code** dropdown list.
  
2. We will move our base image to Alpine Linux.  Open the `Dockerfile` and replace all the context ith the following
file:

```text
FROM eclipse-temurin:17-jdk-alpine as build
WORKDIR /workspace/app 

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

#Pull in security fixes
RUN apk update && apk upgrade

RUN ./mvnw install -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

FROM eclipse-temurin:17-jdk-alpine
VOLUME /tmp  

#Pull in security fixes
RUN apk update && apk upgrade

ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
ENV PORT 5000
EXPOSE $PORT
ENTRYPOINT ["java","-cp","app:app/lib/*","-Dserver.port=${PORT}","com.example.demo.DemoApplication"]
```

3. Since we are not doing live scans within this workshop we need to tell our pipeline to utilize pregenerated security scan data.  Open the `.gitlab-ci.yml` file and edit the `SNYK_RESULT_JSON` variable on line 52, which is the variable associated with Container Scanning, to the value `data/snyk-container-2.json`:

```text
    SNYK_RESULT_JSON: data/snyk-container-2.json
```

4. Once added click the source control button on the left hand side, add a quick commit message, then click the **commit** button.

5. Navigate back to the MR and wait for the entire pipeline to finish running.

6. When the pipeline is done running you can see that Security scanning detected no new potential vulnerabilities.  However, we want to verify that we've removed existing vulnerabilities in the container scans.  Click on **View all pipeline findings**.  

7. From the pipeline security scan report we can see that we now have 0 conatiner scanning findings.

8. Navigate back to the MR details.  Since our policy checks pass (no new Critical and/or no more than 3 new High findings) we can merge our fix.  Click the **Merge** button.

9. This will kickoff our pipeline on the main branch.  Wait a few minutes for the pipeline to complete sucessfully. 

# Step 3: Confirm Vulnerability Remediation

1. Use the left hand navigation menu and click through **Secure -\> Vulnerability Report** to view the full report.  Immediately you'll notice that reported findings has dramatically decreased.  Yay, great job!

2. Use the **Group By** toggle and group the findings by **Severity**.  Also, use **Activity** toggle select **_No Longer Detected_**.  

3. Expand the `Critical` section and we see the 3 critical findings are no longer detected.  Check the selection box for the 3 critical vulnerabilities.  Change the **_Status_** dropdown to `Resolved`, add a comment: `Confirmed as resolved due to migration to Alpine linux`, and click **_Change status_**. 

4. The finidings are now removed from our report.  However, we can always review them by toggling the **Status** dropdown to **Resolved**.  We can then view the defails of past findings and view the audit trail, the pipeline that discovered them, and worflow of associated Issue(s) and MR(s), including comments and approvals. 

5. Typically we'd repeat that process for the High, Medium, Low, and Info vulnerabilites, but we'll skip that in this workshop.  


# Step 4: Project Audit Events

1. Using the left hand navigation menu click through **Secure > Audit events** to get the report on all actions taken on the project for the past month. If no events are shown you may need to edit the time frame.
  
2. You should be able to see some of the actions you have taken and where they occurred in the project hierarchy