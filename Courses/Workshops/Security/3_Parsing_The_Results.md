### Take a look at the results of our pipeline running for the merge request, then merge the code to generate the security reports

### Theme

Following our shift left trend we will see how security results are included throughout each step of the deployment cycle.

# Step 1: Merge Request Security Results

1. Use the left hand navigation menu to click back into **Merge requests** and then click the merge request we created earlier.

2. Right below the approvals section we can see that the results from all of our security scanners we included have generated full security reports unique to this branch for us to view. These reports are generated with each commit so we always know if we have introduced a new bug before its deployed out and disaster strikes.

3. Go ahead and take some time to expand each report and look through the results, then in the **_Security scanning_** section click on any of the critical/high vulnerabilities.
  
4. From this view we can see exactly where it occurred in the code & read up on why its a risk. After taking a look at this we can click the comment icon at the bottom to state that we checked it out and are not worried, then click **Add comment & dismiss**.

5. Next we will want to scroll down and merge our code. At the bottom of the request click **Merge** to kick off our pipeline.
  
6. Once merged use the left hand navigation menu to click through **Build \> Pipelines** and click into the most recently kicked off pipeline. At this point you will go on a quick break until the pipeline completes.

# Step 2: Parsing the Results

1. Now that your **_main_** pipeline has completed the reports under **_Security & Compliance_** have been generated. These reports will only be generated if you run a pipeline against main.
  
2. Use the left hand navigation menu to click through **Secure-\> Security Dashboard**. This will bring up a dashboard view of all of the security vulnerabilities & their counts over time so you can track your work as you secure your project. This dashboard takes a few minutes to collect data so if yours still has no results your presenter will show you the dashboard of a deployed Tanuki Racing application [here](https://gitlab.com/gitlab-learn-labs/webinars/tanuki-racing/tanuki-racing-application/-/security/dashboard)
  
3. We have already seen how to view the vulnerabilities in the pipeline view, but now lets use the left hand navigation menu and click through **Secure -\> Vulnerability Report** to view the full report
  
4. First change the _All tools_ section under **Tools** to just filter on SAST. We can then click into any of the SAST vulnerabilities shown.
  
5. Inside the vulnerability we will want to click the _Explain vulnerability_ button within the **Explain this vulnerability and how to mitigate it with AI** section. This will result in a popup appearing on the right hand side with some information on what the vulnerability is and how you can fix it. The Explain This Vulnerability feature currently works on any SAST vulnerabilities.

# Step 3: Preventive Security Policies

1. Our intial security scans surfaced many security issues in our project's default branch.  To ensure we don't introduce more, we'll create a policy that defines a workflow for preventing vulnerabilites from making it to production.  
  
2. Use the left hand navigation menu to click through **Secure \> Policies** and then click **New policy**. On the resulting page click **Select policy** under **_Merge Request Approval Policy_**.
  
3. Add a **name**, such as `No Critical & Minimal High` to the policy, then under the **_Rules_** section we want to select **Security Scan** in the **When** dropdown list. Then we want to change **All scanners** to be **_Container Scanning_** and **All protected branches** to **default branch**.  Additionally, change the _find(s)_ dropdown list to **More Than** and enter **3** in the text box.  Change the **Severity** to only select **_High_**.

4. Click the **_+ Add Rule_** beneath the rule you just created.  Select **Security Scan** in the **When** dropdown list.  Leave **_All Scanners_** as is, but change **All protected branches** to **default branch**. Change the **Severity** to only select **_Critical_**.
  
5. Then under actions choose **individual users** as the **_Choose approver type_** and add **_lfstucker_** as the required approver.

> Please ensure you do this step or you will be blocked later on in the workshop

6. Lastly click **Configure with a merge request**. On the resulting merge request click ***merge*** and you will be brought to your new policy project that is applied to our workshop application. If you were to create another merge request with the leaked token still in the code based merging would be prevented until it was removed or you added your approval.
  
7. Lastly use the breadcrumbs at the top of the screen to click into your group, then once again click into your project.

> [Docs for policies](https://docs.gitlab.com/ee/user/application_security/policies/)

# Step 5: Security Result Policy In Action

1. Now we want to see our scan results policy in action. From the main page of our project lets go ahead and click **Web IDE** in the **Edit** dropdown list.
  
2. Browse to **_src/main/java/com/example/demo_** and add a new file named `NewFeatureController.java`.  Paste these contents into the file:

```java
package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class NewFeatureController {

    private static Logger LOG = LoggerFactory.getLogger(NewFeatureController.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    //implement a REST GET method that reverses a String
    @RequestMapping(value = "/reverse/{input}", method = RequestMethod.GET)
    public String reverseString(@PathVariable String input) {
        LOG.info("Reversing: " + input);
        StringBuilder reversed = new StringBuilder(input).reverse();
        return reversed.toString();
    }

}
```
  
4. Since we are not doing live scans within this workshop we need to tell our pipeline to utilize pregenerated security scan data.  Open the `.gitlab-ci.yml` file and edit the `SNYK_RESULT_JSON` variable on line 63, which is the variable associated with SAST, to the value `data/snyk-sast-2.json`:

```text
    SNYK_RESULT_JSON: data/snyk-sast-2.json
```

5. Once added click the source control button on the left hand side, add a quick commit message, then click the **down arrow**.
  
6. On the resulting drop down click **Yes** to open a new branch, then click the **_Enter_** key. A new popup will appear where we want to then click **Create MR**

7. Scroll to the bottom, uncheck **_Delete source branch when merge request is accepted_**, and click **Create merge request**
  
8. On the resulting MR notice that our policy requires approval from **_lfstucker_** and is blocked by our policies before we are able to merge. Wait for the entire pipeline to finish running.

9. When the pipeline is done running you can see that our scan result policy have been enacted restricting us from committing this code. We either need to remediate our code or invoke an exception approval to let it pass through the policy. (you can come back to this merge request to review the blocked merge after the next section)
