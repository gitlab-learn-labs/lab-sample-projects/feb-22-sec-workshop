import os
import json
from datetime import datetime

snyk_results = os.getenv('SNYK_RESULT_JSON', 'results.json')
parsing_output = os.getenv('OUTPUT_JSON', 'external-security-results.json')

gl_results = {
    'version': '15.0.7',
    'vulnerabilities': [],
    'remediations': [],
    'scan': {
        "analyzer": {
            "id": "snyk-container",
            "name": "Snyk",
            "url": "https://docs.snyk.io/scan-with-snyk/snyk-container",
            "vendor": {
                "name": "Snyk"
            },
            "version": "1.0.2"
        },
        "scanner": {
            "id": "snyk-container",
            "name": "Snyk",
            "url": "https://docs.snyk.io/scan-with-snyk/snyk-container",
            "vendor": {
                "name": "Snyk"
            },
            "version": "1.0.2" 
        },
    }
}

def parse_links(links):
    parsed_links = []
    for link in links:
        if(link['url'].startswith('http')) :
            parsed_links.append({
                'url': link['url'],
            })
    return parsed_links

def parse_cvss(cvss_list):
    parsed_cvss = []
    for cvss in cvss_list:
        parsed_cvss.append({
            'vendor': cvss['assigner'],
            'vector': cvss['cvssV3Vector'],
        })
    return parsed_cvss

def parse_identifiers(identifiers):
    parsed_identifiers = []

    # https://docs.gitlab.com/ee/development/integrations/secure.html#identifiers
    # CWE
    for identifier in identifiers['CWE']:
        parsed_identifiers.append({
            'type': 'CWE',
            'name': identifier,
            'value': identifier,
            # 'url': identifier['url'],
        })
    # CVE
    for identifier in identifiers['CVE']:
        parsed_identifiers.append({
            'type': 'CVE',
            'name': identifier,
            'value': identifier,
            # 'url': identifier['url'],
        })

     #Alternative???

    return parsed_identifiers

def generate_solution(finding):
    result = "unkown"
    if finding['isUpgradable']:
        result = "Upgrade to version(s) {}".format(finding['fixedIn'])
    return result

def generate_id(finding):
    return finding['id'] + '_' + finding['packageName'] + ":" + finding['version'] + "_" + finding['dockerBaseImage']

with open(snyk_results, 'r') as r:
    start_time = datetime.now()
    print("Beginning parse run: {}".format(start_time))
    
    results_json = json.load(r)   
    print("Snyk findings count: {}".format(len(results_json['vulnerabilities'])))
    image_name = results_json['path']
    for finding in results_json['vulnerabilities']:
        # print("Processing finding: {}".format(finding))

        snyk_severity = finding['severity']
        sev = snyk_severity[0].upper() + snyk_severity[1:]
        gl_results['vulnerabilities'].append({
            "id": generate_id(finding),
            "category": 'container_scanning',
            "name": finding['title'],
            "message": finding['packageName'] + '' + finding['title'],
            "description": finding['description'][:14595],  #Bug where > than 15k characters failing
            "severity": sev,
            "scanner": {
                "id": "snyk_container",
                "name": "Snyk Container"
            },
            'location': {
                'dependency': {
                    'package': {
                        'name': finding['packageName']
                    },
                    'version': finding['version']
                },
                'operating_system': finding['dockerBaseImage'],
                'image': image_name,
            },
            'identifiers': parse_identifiers(finding['identifiers']),
            'links': parse_links(finding['references']),
            'cvss_vectors': parse_cvss(finding['cvssDetails']),
            'solution': generate_solution(finding)
        })

    #Result and Timestamps        
    gl_results['scan']["start_time"]= start_time.strftime('%Y-%m-%dT%H:%M:%S')  #"2023-12-28T14:34:41"
    gl_results['scan']["end_time"] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    gl_results['scan']["status"] = "success"
    gl_results['scan']["type"] = "container_scanning"

    # We need to make sure it is valid json
    try:
        json_object = json.loads(json.dumps(gl_results))
        print ("Is valid json? TRUE")
    except ValueError as e:
        print ("Is valid json? FALSE")

with open(parsing_output, 'w') as f:
    f.write(json.dumps(gl_results, indent=4))

    

